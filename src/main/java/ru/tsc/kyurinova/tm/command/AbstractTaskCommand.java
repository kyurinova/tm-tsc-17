package ru.tsc.kyurinova.tm.command;

import ru.tsc.kyurinova.tm.exception.empty.EmptyNameException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.Date;
import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(Task task) {
        final List<Task> tasks = serviceLocator.getTaskService().findAll();
        final Integer indexNum = tasks.indexOf(task) + 1;
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project id: " + task.getProjectId());
    }

    protected Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Task(name, description, new Date());
    }
}
