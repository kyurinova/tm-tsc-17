package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class TaskShowAllFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-show-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println(serviceLocator.getProjectTaskService().findAllTaskByProjectId(projectId));
    }
}
