package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class TaskRemoveAllFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "tasks-remove-from-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove tasks from project by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(projectId);
    }
}
