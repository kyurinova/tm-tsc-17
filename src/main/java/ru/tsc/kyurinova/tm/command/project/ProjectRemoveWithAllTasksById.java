package ru.tsc.kyurinova.tm.command.project;

import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class ProjectRemoveWithAllTasksById extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeById(projectId);
    }
}
