package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class TaskIsBindToProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-bind-to-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(taskId) == null) throw new TaskNotFoundException();
        final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(projectId, taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

}
