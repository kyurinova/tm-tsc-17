package ru.tsc.kyurinova.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskFromProjectById();

    void removeAllTaskByProjectId();

    void removeProjectById();

    void findAllTaskByProjectId();
}
